﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TraTask.Exceptions;
using TraTask.Models;
using TraTask.Models.Dtos;
using TraTask.Models.Entities;

namespace TraTask.Services
{
    public interface IUserService
    {
        LoginDto Authenticate(LoginDto loginDto);
        UserDto Create(UserCreateDto userCreateDto);
    }
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;
        private readonly AppSettings _appSettings;
        private readonly IMapper _mapper;

        public UserService(IOptions<AppSettings> appSettings,ApplicationDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _context = context;
        }
        public LoginDto Authenticate(LoginDto loginDto)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == loginDto.Email && x.Password == loginDto.Password);
            if (user==null)
            {
                throw new EmailOrPasswordInvalidException();
            }
            
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
          
            var token = tokenHandler.CreateToken(tokenDescriptor);
            loginDto.Token = tokenHandler.WriteToken(token);
            loginDto.Password = null;
            return loginDto;
        }

        public UserDto Create(UserCreateDto userCreateDto)
        {
            var user = _mapper.Map<User>(userCreateDto);
            _context.Users.Add(user);
            _context.SaveChanges();
            return _mapper.Map<UserDto>(user);
        }
    }
}

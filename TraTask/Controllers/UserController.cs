﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraTask.Models.Dtos;
using TraTask.Services;

namespace TraTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]  
        public IActionResult Create(UserCreateDto user)
        {
            var userDto = _userService.Create(user);
            return Ok(userDto);
        } 
    }
}

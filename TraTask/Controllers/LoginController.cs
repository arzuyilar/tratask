﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraTask.Models;
using TraTask.Models.Dtos;
using TraTask.Models.Entities;
using TraTask.Services;

namespace TraTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        public LoginController(ApplicationDbContext context, IMapper mapper,IUserService userService)
        {
            _userService = userService;
            _mapper = mapper;
            _context = context;
        }

        [HttpPost]
        public IActionResult Login([FromForm]LoginDto userDto)
        {
            if(userDto != null)
            {
                var user = _userService.Authenticate(userDto);
                return Ok(user);
            }
            throw new NullReferenceException();   
            
        }
    }
}

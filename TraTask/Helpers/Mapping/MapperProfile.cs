﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraTask.Models.Dtos;
using TraTask.Models.Entities;

namespace TraTask.Mapping
{
    public class MapperProfile:Profile
    {
        public MapperProfile()
        {
            CreateMap<LoginDto, User>();
            CreateMap<UserCreateDto, User>();
            CreateMap<User, UserDto>();
        }
    }
}

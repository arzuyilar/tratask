﻿using AutoWrapper.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TraTask.Exceptions
{
    public class EmailOrPasswordInvalidException : ApiException
    {
        
        public EmailOrPasswordInvalidException(string message = "Email or password is not valid!") : base(message,404)
        {

        }
    }
}
